# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-03-17 11:53+0100\n"
"PO-Revision-Date: 2014-07-17 17:07+0100\n"
"Last-Translator: \n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Office suite\"]]\n"
msgstr "[[!meta title=\"Suite bureautique\"]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"Tails includes <span class=\"application\">[LibreOffice](http://www.libreoffice.org/)</span>,\n"
"a powerful office suite which embeds several tools:\n"
msgstr ""
"Tails inclut <span class=\"application\">[LibreOffice](http://www.libreoffice.org/)</span>,\n"
"une suite bureautique puissante qui comprend plusieurs outils :\n"

#. type: Bullet: '  - '
msgid "**<span class=\"application\">Writer</span>**, a word processor"
msgstr "**<span class=\"application\">Writer</span>**, un traitement de texte"

#. type: Bullet: '  - '
msgid "**<span class=\"application\">Calc</span>**, a spreasheet application"
msgstr "**<span class=\"application\">Calc</span>**, un tableur"

#. type: Bullet: '  - '
msgid "**<span class=\"application\">Impress</span>**, a presentation engine"
msgstr ""
"**<span class=\"application\">Impress</span>**, un créateur de présentations"

#. type: Bullet: '  - '
msgid ""
"**<span class=\"application\">Draw</span>**, a drawing and flowcharting "
"application"
msgstr ""
"**<span class=\"application\">Draw</span>**, une application de dessin et de "
"création d'organigrammes"

#. type: Bullet: '  - '
msgid "**<span class=\"application\">Math</span>**, for editing mathematics"
msgstr ""
"**<span class=\"application\">Math</span>**, pour éditer des mathématiques"

#. type: Plain text
#, no-wrap
msgid ""
"To start <span class=\"application\">LibreOffice</span> choose\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">Office</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">LibreOffice</span></span>.\n"
msgstr ""
"Pour démarrer <span class=\"application\">LibreOffice</span> choisissez\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">Bureautique</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">LibreOffice</span></span>.\n"

#. type: Plain text
#, no-wrap
msgid ""
"The <span class=\"application\">LibreOffice</span> website provides complete [user\n"
"guides](https://wiki.documentfoundation.org/Documentation/Publications) for each\n"
"of these tools, translated into several languages.\n"
msgstr ""
"Le site web de <span class=\"application\">LibreOffice</span> offre des [guides\n"
"utilisateur](https://wiki.documentfoundation.org/Documentation/Publications/fr) complets pour chacun\n"
"de ces outils, traduits en plusieurs langues.\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/sensitive_documents/persistence\" raw=\"yes\"]]\n"
msgstr "[[!inline pages=\"doc/sensitive_documents/persistence.fr\" raw=\"yes\"]]\n"

#. type: Plain text
#, no-wrap
#| msgid "[[!inline pages=\"doc/sensitive_documents/persistence\" raw=\"yes\"]]\n"
msgid "[[!inline pages=\"doc/sensitive_documents/metadata\" raw=\"yes\"]]\n"
msgstr "[[!inline pages=\"doc/sensitive_documents/metadata.fr\" raw=\"yes\"]]\n"

#~ msgid ""
#~ "Tails includes [LibreOffice](http://www.libreoffice.org/), which is a "
#~ "full-featured office productivity suite that provides a near drop-in "
#~ "replacement for Microsoft(R) Office."
#~ msgstr ""
#~ "Tails contient [LibreOffice](https://fr.libreoffice.org/), qui est une "
#~ "solution bureautique complète et multi–fonction. Il est un substitut très "
#~ "proche de Microsoft(R) Office."

#~ msgid ""
#~ "It includes a word processor, a spreadsheet and a presentation "
#~ "application."
#~ msgstr ""
#~ "Cette suite inclut un traitement de texte, un tableur et un gestionnaire "
#~ "de présentation et de création de diaporamas."

#~ msgid "You can launch them from the *Applications*&nbsp;▸ *Office*"
#~ msgstr "Vous pouvez les lancer via *Applications*&nbsp;▸ *Bureautique*"
